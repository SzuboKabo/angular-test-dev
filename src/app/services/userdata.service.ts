import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../models/post.model';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  constructor(private http: HttpClient) {}
  private serviceUrl = 'https://jsonplaceholder.typicode.com/posts';

  getPost(): Observable<Post[]> {
    return this.http.get<Post[]>(this.serviceUrl);
  }
}
