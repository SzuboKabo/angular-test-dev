import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

import { Post } from 'src/app/models/post.model';
import { PostService } from 'src/app/services/userdata.service';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'userdatatable',
  templateUrl: './userdatatable.component.html',
  styleUrls: ['./userdatatable.component.css'],
})
export class UserdatatableComponent implements OnInit {
  posta: Post[];
  dataSource: MatTableDataSource<Post>;
  searchKey: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private postService: PostService) {}

  displayedColumns = ['userId', 'id', 'title', 'body', 'actions'];

  ngOnInit(): void {
    this.getPost();
  }

  getPost(): void {
    this.postService.getPost().subscribe((posta) => {
      this.posta = posta;
      console.log(this.posta);
      this.dataSource = new MatTableDataSource(this.posta);
      this.dataSource.paginator = this.paginator;
    });
  }

  displayDetail = false;
  displayTable = true;
  detailId = '';
  detailBody = '';
  detailTitle = '';
  detailUserId = '';

  getRecord(row) {
    console.log(row);
    this.detailUserId = row.userId;
    this.detailId = row.id;
    this.detailTitle = row.title;
    this.detailBody = row.body;
  }

  showHideTable() {
    this.displayDetail = !this.displayDetail;
    this.displayTable = !this.displayTable;
  }

  onSearchClear() {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
  }
}
